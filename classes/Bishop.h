#pragma once
#include "Piece.h"
class Bishop : public Piece
{
private:

public:
	virtual char checkDest(std::string dest, Piece ** gameBoard); //check if Destination is ligal move function
	Bishop(char type, char* dest);// bishop object constractor
	virtual ~Bishop();// distractor
};

