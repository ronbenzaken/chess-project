#pragma once
#include <iostream>
#include "Board.h"
#define PLACE_SIZE 3
class Board;
class Piece
{
protected:
	char _type;
	char place[PLACE_SIZE];
	bool _firstStep;
public:
	Piece(const char type, const char * place);
	virtual char checkDest(std::string dest, Piece** gameBoard ) = 0;// Check destinations
	//bool checkSameSoldiar(std::string dest, Piece** gameBoard);
	void setPlace(const char * dest);// function to set the place of the piece gives string and returns nothing
	char* getPlace(void);// function to get the place of the piece gives nothing and returns string
	char getType(); //function to get the type of the piece
	void setFirstStep();// function to set the first position of the piece
};
