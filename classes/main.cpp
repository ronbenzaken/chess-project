#include <string>
#include "Manager.h"
#include "King.h"
#include "Pawn.h"
#include "Knight.h"
#include "Bishop.h"
#include "Rook.h"
#include "Queen.h"
//the board id including all the pieces 
using std::string;
int main()
{
	
	srand(time_t(NULL));
	Manager game;
	Pipe* p = game.getGame();
	Board* gameBoard = game.getGameBoard();
	Piece** board = gameBoard->getBoard();
	bool isConnect = p->connect(); // check if connectd to graphics if so the program will run
	string ans;
	char ansFromBoard[2];
	while (!isConnect)// enter loop while the program is not connected to graphics
	{
		cout << "cant connect to graphics" << endl; // message to user
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;// take answer input from user to try again or exit

		if (ans == "0") // iff user entered "0" the program will try to connect again and write message as well
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p->connect();
		}
		else
		{
			p->close();
			return 0;
		}
	}


	char * msgToGraphics;
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	msgToGraphics = gameBoard->gameBoardStr();
	p->sendMessageToGraphics(msgToGraphics);   // send the board string
	delete (msgToGraphics);						   // get message from graphics
	string msgFromGraphics = p->getMessageFromGraphics();
	char place[2];
	char dest[2];
	place[0] = msgFromGraphics[0];
	place[1] = msgFromGraphics[1];
	dest[0] = msgFromGraphics[2];
	dest[1] = msgFromGraphics[3];
	while (msgFromGraphics != "quit")
	{
		
		ansFromBoard[0] = gameBoard->checkMove(place, dest);	// return result to graphics
		ansFromBoard[1] = 0;
		p->sendMessageToGraphics(ansFromBoard);
		msgFromGraphics = p->getMessageFromGraphics();
		place[0] = msgFromGraphics[0];
		place[1] = msgFromGraphics[1];
		dest[0] = msgFromGraphics[2];
		dest[1] = msgFromGraphics[3];
	}

	p->close();
}




