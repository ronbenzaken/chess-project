#include "Board.h"
#include "Empty.h"
#include "King.h"
#include "Bishop.h"
#include "Pawn.h"
#include "Knight.h"
#include "Queen.h"
#include "Rook.h"

Board::Board()
{
	this->_turn = ZERO;// white start
	int i = 0, j = 0;
	char place[2] = {0};// temp place for all pieces
	for (j = 0; j < (BOARD_SIZE/2)+1; j++)// start up the firs line of white troops
	{
		switch (j)
		{
		case ROOK:
			place[0] = j + LITTLE_A;
			place[1] = i + ONE;
			this->_board[i][j] = new Rook(WHITE_ROOK, place);
			place[0] = BOARD_SIZE -1 - j + LITTLE_A;
			this->_board[i][BOARD_SIZE - 1 - j] = new Rook(WHITE_ROOK, place);
			break;
		case KNIGHT:
			place[0] = j + LITTLE_A;
			place[1] = i + ONE;
			this->_board[i][j] = new Knight(WHITE_KNIGHT, place);
			place[0] = BOARD_SIZE - 1 - j + LITTLE_A;
			this->_board[i][BOARD_SIZE - 1 - j] = new Knight(WHITE_KNIGHT, place);
			break;
		case BISHOP:
			place[0] = j + LITTLE_A;
			place[1] = i + ONE;
			this->_board[i][j] = new Bishop(WHITE_BISHOP, place);
			place[0] = BOARD_SIZE - 1 - j + LITTLE_A;
			this->_board[i][BOARD_SIZE - 1 - j] = new Bishop(WHITE_BISHOP, place);
			break;
		case QUEEN:
			place[0] = j + LITTLE_A;
			place[1] = i + ONE;
			this->_board[i][j] = new Queen(WHITE_QUEEN, place);
			break;
		case KING:
			place[0] = j + LITTLE_A;
			place[1] = i + ONE;
			this->_board[i][j] = new King(WHITE_KING, place);
			break;
		}	
	}
	i = 1;// second line
	for (j = 0; j < BOARD_SIZE; j++)// set up all second line with white pawn
	{
		place[0] = j + LITTLE_A;
		place[1] = i + ONE;
		this->_board[i][j] = new Pawn(WHITE_PAWN, place);
	}
	
	for (i = 2; i < BOARD_SIZE - 2; i++)// set up all 2-6 line with empty piece
	{
		for (j = 0; j < BOARD_SIZE; j++)
		{
			place[0] = j + LITTLE_A;
			place[1] = i + ONE;
			this->_board[i][j] = new Empty(EMPTY, place);
		}
	}
	
	for (j = 0; j < BOARD_SIZE; j++)// set up all second line from the end with black pawn
	{
		place[0] = j + LITTLE_A;
		place[1] = i + ONE;
		this->_board[i][j] = new Pawn(BLACK_PAWN, place);
	}
	i++;// end line
	for (j = 0; j <= (BOARD_SIZE / 2) + 1; j++)// set up all end line with black troops
	{
		switch (j)
		{
		case ROOK:
			place[0] = j + LITTLE_A;
			place[1] = i + ONE;
			this->_board[i][j] = new Rook(BLACK_ROOK, place);
			place[0] = BOARD_SIZE - 1 - j + LITTLE_A;
			this->_board[i][BOARD_SIZE - 1 - j] = new Rook(BLACK_ROOK, place);
			break;
		case KNIGHT:
			place[0] = j + LITTLE_A;
			place[1] = i + ONE;
			this->_board[i][j] = new Knight(BLACK_KNIGHT, place);
			place[0] = BOARD_SIZE - 1 - j + LITTLE_A;
			this->_board[i][BOARD_SIZE - 1 - j] = new Knight(BLACK_KNIGHT, place);
			break;
		case BISHOP:
			place[0] = j + LITTLE_A;
			place[1] = i + ONE;
			this->_board[i][j] = new Bishop(BLACK_BISHOP, place);
			place[0] = BOARD_SIZE - 1 - j + LITTLE_A;
			this->_board[i][BOARD_SIZE - 1 - j] = new Bishop(BLACK_BISHOP, place);
			break;
		case QUEEN:
			place[0] = j + LITTLE_A;
			place[1] = i + ONE;
			this->_board[i][j] = new Queen(BLACK_QUEEN, place);
			break;
		case KING:
			place[0] = j + LITTLE_A;
			place[1] = i + ONE;
			this->_board[i][j] = new King(BLACK_KING, place);
			break;
		}
	}

}
Board::~Board()
{
	int i = 0, j = 0;
	for (i = 0; i < BOARD_SIZE; i++)//free all the memory in array
	{
		for (j = 0; j < BOARD_SIZE; j++)
		{
			delete (this->_board[i][j]);
		}
	}
}
/*
return all the board str from the end from h1 - a8
*/
char* Board::gameBoardStr()
{
	int i = 0, j = 0, counter = 0;
	char * str = new char[STR_SIZE];
	char type;
	for (i = BOARD_SIZE - 1; i >= 0; i--)
	{
		for (j = 0; j < BOARD_SIZE; j++)
		{
			str[counter] = this->_board[i][j]->getType();
			counter++;
		}
	}
	str[BOARD_SIZE*BOARD_SIZE] = this->_turn;// add the turn
	str[BOARD_SIZE*BOARD_SIZE + 1] = 0;// add null
	return str;
}

Piece **Board::getBoard() const
{
	
	return (Piece**)_board;// return the board
}

/*
this function get a board and a color that means witch color king to checks  
*/
bool Board::chess(Piece** board, int color)
{
	bool flag = false;
	char* kingPlace;// the king plac x,y
	int i = 0, j = 0 , kingX = 0, kingY = 0;
	if (color == 1)//to check if there are any therete on white king
	{
		for (i = 0; i < BOARD_SIZE && !flag; i++)// found white king place
		{
			for (j = 0; j < BOARD_SIZE && !flag; j++) 
			{
				if (board[i*(BOARD_SIZE) + j]->getType() == WHITE_KING)
				{
					kingX = j;
					kingY = i;
					flag = true;
				}
			}
		}
		kingPlace = board[kingY*(BOARD_SIZE) + kingX]->getPlace();
		for (i = 0; i < BOARD_SIZE; i++)// move one by one on any black soldiar and check if its threating the white king
		{
			for (j = 0; j < BOARD_SIZE; j++)
			{
				if (board[i*(BOARD_SIZE) + j]->getType() >= LITTLE_A)
				{
					if (board[i*(BOARD_SIZE) + j]->checkDest(kingPlace, board) == VALID_MOVE)
					{
						return true;// if there any threating
					}
				}
			}

		}
		return false;
	}
	else//black king check threating
	{
		for (i = 0; i < BOARD_SIZE && !flag; i++)// found black king place
		{
			for (j = 0; j < BOARD_SIZE && !flag; j++)
			{
				if (board[i*(BOARD_SIZE) + j]->getType() == BLACK_KING)
				{
					kingX = j;
					kingY = i;
					flag = true;
				}
			}
		}
		kingPlace = board[kingY*(BOARD_SIZE) + kingX]->getPlace();
		for (i = 0; i < BOARD_SIZE; i++)// move one by one on any white soldiar and check if its threating the black king
		{
			for (j = 0; j < BOARD_SIZE; j++)
			{
				if (board[i*(BOARD_SIZE ) + j]->getType() < LITTLE_A)
				{
					if (board[i*(BOARD_SIZE) + j]->checkDest(kingPlace, board) == VALID_MOVE)
					{
						return true;// if there any threating
					}
				}
			}


		}
		return false;
	}

}

char Board::checkMove(char* place, char* dest)
{
	char ans;
	char currentTurn = this->_turn;
	char savePlace[2];
	Piece * save1;
	Piece * save2;
	int turn = 0;
	if (this->_turn == ZERO)// to enter the color by int 1 == white == ZERO
	{
		turn = 1;
	}
	int x, y, x2, y2;
	x = place[0] - LITTLE_A;
	y = place[1] - ONE;
	x2 = dest[0] - LITTLE_A;
	y2 = dest[1] - ONE;
	
	if ((currentTurn == ZERO && this->_board[y][x]->getType() < LITTLE_A) || (currentTurn != ZERO && this->_board[y][x]->getType() > LITTLE_A))//check if its the current player turn 
	{
		if (x >= BOARD_SIZE || y >= BOARD_SIZE || x2 >= BOARD_SIZE || y2 >= BOARD_SIZE || x < 0 || y < 0 || x2 < 0 || y2 < 0)// check if its out of bounds
		{
			ans = OUT_OF_BOARD; 
			return ans;
		}
		else if (this->checkSameSoldiar(place, dest))// if in the dest has the same color troop
		{
			ans = DEST_SAME_PLAYER;
			return ans;
		}
		else if (x == x2 && y == y2)// check if the same place
		{
			ans = SAME_PLACE;
			return ans;
		}
		ans = this->_board[y][x]->checkDest(dest, this->getBoard());
		if (ans == VALID_MOVE)// check if we can move their by his place
		{
			save1 = this->_board[y][x];
			this->_board[y][x] = new Empty(EMPTY, place);
			save2 = this->_board[y2][x2];
			this->_board[y2][x2] = save1;
			if (this->_board[y2][x2]->getType() == WHITE_KING || this->_board[y2][x2]->getType() == BLACK_KING) // if we are moving the king we need to move his place at first
			{
				savePlace[0] = this->_board[y2][x2]->getPlace()[0];
				savePlace[1] = this->_board[y2][x2]->getPlace()[1];
				this->_board[y2][x2]->setPlace(dest);//set the new place
			}
			if (chess((Piece**)_board, turn))// check if after moving there isnt chess on the player that his turn 
			{
				if (this->_board[y2][x2]->getType() == WHITE_KING || this->_board[y2][x2]->getType() == BLACK_KING)//if there are chess invalid move
				{
					this->_board[y2][x2]->setPlace(savePlace);// return the previous place to king
				}
				ans = MAKE_CHESS;
				delete (this->_board[y][x]);
				this->_board[y][x] = save1;
				this->_board[y2][x2] = save2;
				return ans;
			}
			else// valid move
			{
				delete(save2);// delete the new soldiar that has in that place
				this->_board[y2][x2]->setPlace(dest);
			}
			if (this->_turn == ZERO)//change turn
			{
				this->_turn++;
			}
			else
			{
				this->_turn = ZERO;
			}
			if (chess((Piece**)_board, turn + 1))// check if the move make a chess
			{
				ans = VALID_MOVE_CHESS;
				this->_board[y2][x2]->setFirstStep();
				return ans;
			}
			this->_board[y2][x2]->setFirstStep();
			
			ans = VALID_MOVE;
			return ans;
		}
	}
	else// if its not his turn
	{
		ans = ISNT_CURRRENT_PLAYER;
		return ans;
	}
	return ans;
		
}
/*
if there are same soldiar in both place and dest its return true
output: boolean true/false
input: place and dest
*/
bool Board::checkSameSoldiar(char * place, char* dest)
{
	int x = 0, y = 0, x2 = 0, y2 = 0;
	bool ans = false;
	x = place[0] - LITTLE_A;
	y = place[1] - ONE;
	x2 = dest[0] - LITTLE_A;
	y2 = dest[1] - ONE;
	if (this->_board[y][x]->getType() >= LITTLE_A)// black
	{
		if (this->_board[y2][x2]->getType() >= LITTLE_A)
		{
			ans = true;
			return ans;
		}
	}
	else
	{
		if (this->_board[y2][x2]->getType() < LITTLE_A && this->_board[y2][x2]->getType() != EMPTY)
		{
			ans = true;
			return ans;
		}
	}
	return ans;
}