#pragma once
#include "Piece.h"
class Knight : public Piece
{
private:

public:
	virtual char checkDest(std::string dest, Piece ** gameBoard);// Check destinations
	Knight(const char type, char* dest);// "knight" constructor
	virtual ~Knight();// "knight" distructor
};