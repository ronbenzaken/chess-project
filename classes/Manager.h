#pragma once
#include "Pipe.h"
#include "Board.h"
class Manager
{
private:
	Board _gameBoard;
	Pipe _game;
	
public:
	Board* getGameBoard();
	Pipe* getGame();
	
};
