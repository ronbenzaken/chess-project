#include "Rook.h"
Rook::Rook(const char type, char* place): Piece(type, place)
{

}
Rook::~Rook()
{

}
char Rook::checkDest(std::string dest, Piece ** gameBoard)
{

	int x = 0, y = 0, x2 = 0, y2 = 0, i = 0, j = 0;
	char ans = CANT_GO_THERE;
	char type = this->_type;
	x = this->place[0] - LITTLE_A;
	y = this->place[1] - ONE;
	x2 = dest[0] - LITTLE_A;
	y2 = dest[1] - ONE;
	if (x == x2 || y == y2)
	{
		if (x > x2)
		{
			for (i = -1; x + i >= 0; i--)
			{
				if (x + i == x2)
				{
					ans = VALID_MOVE;
					return ans;
				}
				if (gameBoard[y*BOARD_SIZE + x + i]->getType() != EMPTY)
				{
					return ans;
				}
				
			}
		}
		else if (x < x2)
		{
			for (i = 1; x + i < BOARD_SIZE; i++)
			{
				if (x + i == x2)
				{
					ans = VALID_MOVE;
					return ans;
				}
				if (gameBoard[y*BOARD_SIZE + x + i]->getType() != EMPTY)
				{
					return ans;
				}
				
			}
		}
		else if (y > y2)
		{
			for (i = -1; i + y >= 0; i--)
			{
				if (y + i == y2)
				{
					ans = VALID_MOVE;
					return ans;
				}
				if (gameBoard[(y+i)*BOARD_SIZE + x]->getType() != EMPTY)
				{
					return ans;
				}
				
			}

		}
		else
		{

			for (i = 1; i + y < BOARD_SIZE; i++)
			{
				if (y + i == y2)
				{
					ans = VALID_MOVE;
					return ans;
				}
				if (gameBoard[(y + i)*BOARD_SIZE + x]->getType() != EMPTY)
				{
					return ans;
				}
				
			}
		}
	}
	return ans;
}