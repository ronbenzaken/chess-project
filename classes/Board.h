#pragma once
#include "Piece.h"

#define BOARD_SIZE 8
#define ZERO 48
#define ONE 49
#define STR_SIZE 100
#define A 65
#define LITTLE_A 97
#define AB_LETTERS 26
#define EMPTY '#'
#define WHITE_PAWN 'P'
#define WHITE_BISHOP 'B'
#define WHITE_KNIGHT 'N'
#define WHITE_QUEEN 'Q'
#define WHITE_ROOK 'R'
#define WHITE_KING 'K'
#define BLACK_PAWN 'p'
#define BLACK_BISHOP 'b'
#define BLACK_KNIGHT 'n'
#define BLACK_QUEEN 'q'
#define	BLACK_ROOK 'r'
#define BLACK_KING 'k'
#define VALID_MOVE '0'
#define VALID_MOVE_CHESS '1'
#define ISNT_CURRRENT_PLAYER '2'
#define DEST_SAME_PLAYER '3'
#define MAKE_CHESS '4'
#define OUT_OF_BOARD '5'
#define CANT_GO_THERE '6'
#define SAME_PLACE '7'
#define CHESS_MET '8'
#define ROOK 0
#define KNIGHT 1
#define BISHOP 2
#define QUEEN 3
#define KING 4

class Piece;
class Board
{
private:
	char _turn;
	Piece* _board[BOARD_SIZE][BOARD_SIZE] = {};

public:
	Board();
	~Board();
	char* gameBoardStr();
	Piece** getBoard() const;
	char checkMove(char* place, char* dest);
	bool chess(Piece** board, int color);
	bool checkSameSoldiar(char* place, char* dest);

};