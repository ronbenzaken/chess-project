#include "Pawn.h"
Pawn::Pawn(char type, char* dest) : Piece(type, dest)
{
	
}
Pawn::~Pawn()
{

}
char Pawn::checkDest(std::string dest, Piece ** gameBoard)
{
	int x = 0, y = 0, x2 = 0, y2 = 0, i = 0, j = 0;
	char ans = '6';
	char type = this->_type;
	x = this->place[0] - LITTLE_A;
	y = this->place[1] - ONE;
	x2 = dest[0] - LITTLE_A;
	y2 = dest[1] - ONE;
	if (!(x2 < 0 || x2 >= BOARD_SIZE || y2 < 0 || y2 >= BOARD_SIZE))
	{
		if (x2 == x)
		{
			if (type < LITTLE_A)
			{
				if (this->_firstStep)
				{
					for (i = y + 1; i <= y+2; i++)
					{
						if (gameBoard[i*BOARD_SIZE + x]->getType() == EMPTY)
						{
							if (i == y2)
							{
								ans = '0';
								//this->_firstStep = false;
								return ans;
							}
						}
						else
						{
							return ans;
						}
					}
				}
				else
				{
					if (gameBoard[y2*BOARD_SIZE + x]->getType() == EMPTY && y + 1 == y2)
					{
						ans = '0';
						
						return ans;
					}
					else
					{
						ans = CANT_GO_THERE;
						return ans;
					}
				}
			}
			else
			{
				if (this->_firstStep)
				{
					for (i = y - 1; i >= y - 2; i--)
					{
						if (gameBoard[i*BOARD_SIZE + x]->getType() == EMPTY)
						{
							if (i == y2)
							{
								ans = '0';
								//this->_firstStep = false;
								return ans;
							}
						}
						else
						{
							return ans;
						}
					}
				}
				else
				{
					if (gameBoard[y2*BOARD_SIZE + x]->getType() == EMPTY && y - 1 == y2)
					{
						ans = '0';
						return ans;
					}
					else
					{
						return ans;
					}
				}

			}
		}
		else if (x + 1 == x2)
		{
			if (type < LITTLE_A && y+1 == y2 )
			{
				if (gameBoard[y2* BOARD_SIZE + x2]->getType() != EMPTY )
				{ 
					ans = '0';
					return ans;
				}
			}
			else if(!(type < LITTLE_A) &&  y-1 == y2)
			{
				if (gameBoard[y2 * BOARD_SIZE + x2]->getType() != EMPTY )
				{
					ans = '0';
					return ans;
				}
			}
		}
		else if (x > 0 && x - 1 == x2)
		{
			if (type < LITTLE_A)
			{
				if (gameBoard[y2 * BOARD_SIZE + x2]->getType() != EMPTY  && y + 1 == y2)
				{
					ans = '0';
					return ans;
				}
			}
			else
			{
				if (gameBoard[y2 * BOARD_SIZE + x2]->getType() != EMPTY && y - 1 == y2)
				{
					ans = '0';
					return ans;
				}
			}
		}

		return ans;
			
	}
}