#include "Queen.h"
#include "Rook.h"
#include "Bishop.h"
Queen::Queen(const char type, char* place) :Piece(type, place)
{

}
Queen:: ~Queen()
{

}
char Queen::checkDest(std::string dest, Piece ** gameBoard)
{
	this->_queenBishop = new Bishop(this->_type, this->place);
	this->_queenRook = new Rook(this->_type, this->place);
	if (this->_queenBishop->checkDest(dest, gameBoard) == CANT_GO_THERE && this->_queenRook->checkDest(dest, gameBoard) == CANT_GO_THERE)
	{
		return CANT_GO_THERE;
	}
	return VALID_MOVE;
	delete (this->_queenBishop);
	delete (this->_queenRook);
}