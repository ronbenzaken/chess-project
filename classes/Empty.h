#pragma once
#include "Piece.h"
class Empty : public Piece
{
private:

public:
	Empty(char type, char* dest);// "empty" constructor
	virtual ~Empty();// "empty" distructor
	virtual char checkDest(std::string dest, Piece ** gameBoard);// function to check if destination is legal


};