#include "Bishop.h"
Bishop::Bishop(const char type, char* place) :Piece(type, place)
{

}
Bishop::~Bishop()
{

}
char Bishop::checkDest(std::string dest, Piece ** gameBoard)
{
	int x = 0, y = 0, x2 = 0, y2 = 0, i = 0, j = 0; //Variables
	char ans = CANT_GO_THERE; //return this if can not go to a certain slot
	char type = this->_type; 
	x = this->place[0] - LITTLE_A; // rows starting point 
	y = this->place[1] - ONE; // Columns starting point 
	x2 = dest[0] - LITTLE_A; // rows destination  
	y2 = dest[1] - ONE; // Columns destination
	if (x - x2 == y - y2) // if the difference between the starting point of the rows and Columns and the destinaition then enter
	{
		if (x < x2 && y < y2) // if the bishop is in the bottom left - enter  
		{
			for (i = 1; x + i < BOARD_SIZE && y + i < BOARD_SIZE; i++)// loop and condition to come through all the places of the array from the bottom left to top right
			{
				if (y + i == y2 && x + i == x2) 
				{
					ans = VALID_MOVE;
					return ans;
				}
				if (gameBoard[(y + i)*BOARD_SIZE + x + i]->getType() != EMPTY)// if the cuurent place of the board is not empty(#) enter and return false
				{
					return ans;
				}
				
			}
		}
		else
		{
			for (i = -1; x + i >= 0 && y + i >= 0; i--)// go through all the places that goes from the top right to the bottom left
			{
				if (y + i == y2 && x + i == x2)
				{
					ans = VALID_MOVE;
					return ans;
				}
				if (gameBoard[(y + i)*BOARD_SIZE + x + i]->getType() != EMPTY)// if the cuurent place of the board is not empty(#) enter and return false
				{
					return ans;
				}
				
			}
		}
	}
	else if (x - x2 == -(y - y2))
	{
		if (x > x2 && y < y2)// go through all the places that goes from the top left to the bottom right
		{
			for (i = 1; x - i >= 0 && y + i < BOARD_SIZE; i++)
			{
				if (y + i == y2 && x - i == x2)
				{
					ans = VALID_MOVE;
					return ans;
				}
				if (gameBoard[(y + i)*BOARD_SIZE + x - i]->getType() != EMPTY)// if the cuurent place of the board is not empty(#) enter and return false
				{
					return ans;
				}
				
			}
		}
		else
		{
			for (i = 1; x + i < BOARD_SIZE && y - i >= 0; i++)
			{
				if (y - i == y2 && x + i == x2)
				{
					ans = VALID_MOVE;
					return ans;
				}
				if (gameBoard[(y - i)*BOARD_SIZE + x + i]->getType() != EMPTY)
				{
					return ans;
				}
				
			}
		}
	}
	return ans;
}