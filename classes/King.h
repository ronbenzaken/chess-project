#pragma once
#include "Piece.h"
class King : public Piece
{
private:

public:
	King(char type, char* place); // "king" constructor
	virtual ~King();// "king" distructor
	virtual char checkDest(std::string dest, Piece** gameBoard);// Check destinations
	
};