#pragma once
#include "Piece.h"

class Queen : public Piece // queen class inherit from piece class
{
private:
	Piece * _queenRook; 
	Piece * _queenBishop;
public:
	virtual char checkDest(std::string dest, Piece ** gameBoard);// Check destinations
	Queen(const char type, char* place);
	virtual ~Queen();
};// the queen inherit and know where she can go from the bishop and the rook