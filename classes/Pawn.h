#pragma once
#include "Piece.h"
class Pawn : public Piece
{
private:
	
public:
	virtual char checkDest(std::string dest, Piece ** gameBoard);// Check destinations
	Pawn(char type, char* dest);// "pawn" constructor
	virtual ~Pawn();// "pawn" distructor

};