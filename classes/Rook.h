#pragma once
#include "Piece.h"
class Rook: public Piece// inherit from piece
{
private:

public:
	virtual char checkDest(std::string dest, Piece ** gameBoard);// Check destinations
	Rook(char type, char* dest);// "rook" constructor
	virtual ~Rook();// "rook" distructor
};